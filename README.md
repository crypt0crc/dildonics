# Dildonics

![Alt text](/images/dildonics_image.png)


_Dildonics is a tool developed as proof of concept, for the non-permitted control of "Realov" and "Magic Motion" dildos.
The application implements the option to control the intensity of the vibration , also can turn off the devices._


## Pre requirements 📋

_For running the requirements are :_

```
Bluepy library : wget https://github.com/IanHarvey/bluepy.git
```
```
Gatttool(check installation)
```
```
python
```

## Instalation 🔧

_To install the requirements:_

_Bluepy install debian :_

```
$ sudo apt-get install python-pip libglib2.0-dev
$ sudo pip install bluepy
```

_Bluepy install Fedora :_

```
$ sudo dnf install python-pip glib2-devel
```

_For Python 3, you may need to use pip3:_

```
$ sudo apt-get install python3-pip libglib2.0-dev
$ sudo pip3 install bluepy
```
_For Python 3, you may need to use pip3:_
```
$ sudo apt-get install git build-essential libglib2.0-dev
$ git clone https://github.com/IanHarvey/bluepy.git
$ cd bluepy
$ python setup.py build
$ sudo python setup.py install
```
_An online version of this is currently available at: http://ianharvey.github.io/bluepy-doc/_

_Gatttool install Kali_
```
apt install gatttool
```
_Gatttool install Debian_

```
$ wget https://www.kernel.org/pub/linux/bluetooth/bluez-5.18.tar.xz

$ dpkg --get-selections | grep -v deinstall | grep bluez

$ tar xvf bluez-5.18.tar.xz

$ sudo apt-get install libglib2.0-dev libdbus-1-dev libusb-dev libudev-dev libical-dev systemd libreadline-dev

$ .configure --enable-library

$ make -j8 && sudo make install

$ sudo cp attrib/gatttool /usr/local/bin/
```

## Run the Script ⚙️

_$ python dildonics.py_

### Principal Menu  🔩

_Scan Vulnerable devices_

```
Check if vulnerable devices are available.
```

_Obtain Services and Data_

```
Get services and data of all available devices.
```

_Attack Realov_

```
Launch attack menu for Realov devices. First we need to scan!.
```

_Attack Magic Motion_

```
Launch attack menu for Magic Motion devices. First we need to scan!.
```

_Exit_

```
Close Script and exit.
```
#### Attack Menu Realov 🛠️

_1. Turn on_

```
Turn on the dildo , and select the intensity(1-4).
```

_2. Turn off_

```
Turn of the dildo.
```

_3. Back_

```
Go back to the main menu!.
```

_4. Exit_

```
Close the script and exit.
```

#### Attack Menu Magic motion 🛠️

_1. Turn on_

```
Turn on the dildo , and select the intensity(1-6).
```

_2. Turn off_

```
Turn of the dildo.
```

_3. Back_

```
Go back to the main menu!.
```

_4. Exit_

```
Close the script and exit.
```

## Authors ✒️

_Developed by :_

**David Cámara** - *Initial source code, Code Review and Documentation* - [@crypt0crc,@pieldepanceta]

**Álvaro Temes** - *Initial source code and Code Review* - [@Nimcu]


## License 📄

This project uses code from the bluez project, which is available under the Version 2 of the GNU Public License.
Also thanks to IanHarvey for the bluepy module.

The Python files are released into the public domain by their authors, David Cámara and Álvaro Temes.

## Thanks us 🎁

* Share the project 📢
* Invite a beer to us 🍺. 
* Give us public thanks 🤓.


